'use strict';

function scrapeCDotLicense(url) {
  let info = {};
  info.url = url;

  let dt = document.querySelectorAll('div.content div.fieldset dl dt');
  let dd = document.querySelectorAll('div.content div.fieldset dl dd');
  if (dt.length && (dt.length == dd.length)) {
    for (let i = 0; i < dt.length; i++) {
      // Field name, eg 'First name' -> remove LF/CR, lowercase, snake_case -> 'first_name'
      let key = dt[i].textContent.trim().toLowerCase().replace(/ /g, '_');
      let value = dt[i].nextElementSibling.textContent.trim();
      info[key] = value;
    }
  } else {
    notifyError('ERROR: dt and dd length not equal');
  }
  return pasteCDotLicense(info);
}

function pasteCDotLicense(info) {
  let text =
    `URL = ${info.url}\n` +
    `Name = ${info.name}\n` +
    `Company = ${info.company}\n` +
    `Email = ${info.email}\n` +
    `Issued at = ${info.issued_at}\n` +
    `Starts at = ${info.starts_at}\n` +
    `Expires at = ${info.expires_at}\n` +
    `Licensed users count = ${info.users_count}\n` +
    `Previous users count = ${info.previous_users_count}\n` +
    `Trueup count = ${info.trueup_count}\n` +
    `Zuora sub = ${info.zuora_subscription}\n` +
    `Zuora name = ${info.zuora_subscription_name}\n` +
    `Trial = ${info.trial}\n` +
    `Notes = ${info.notes}\n` +
    `Plan = ${info.gitlab_plan}\n` +
    `Creator = ${info.creator}\n`
  ;
  return text;
}

