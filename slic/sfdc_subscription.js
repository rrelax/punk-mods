'use strict';

function scrapeSfdcSubscription(url) {
  let info = {};
  info.url = url;
  console.log('SFDC Subscription page');
  return pasteSfdcSubscription(info);
}

function pasteSfdcSubscription(info) {
  let text =
    `URL = ${info.url}\n`
  ;
  return text;
}
