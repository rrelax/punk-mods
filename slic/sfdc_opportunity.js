'use strict';

function labelToValue(search_label, labelNodes) {
  // I have to stoopidly loop, as there's no 1:1 relation between labels and values indexes...
  let value = '???';
  for (const node of labelNodes) {
    // outerText gets rid of the helpbutton stuff
    if (node.outerText === search_label) {
      value = node.nextSibling.textContent;
      break;
    }
  }
  return value;
}

function scrapeSfdcOpportunity(url) {
  let info = {};
  info.url = url;
  let labelNodes = document.querySelectorAll('.detailList tr td.labelCol');
  info.opportunity_name = labelToValue('Opportunity Name', labelNodes);
  info.account_name = labelToValue('Account Name', labelNodes);
  info.type = labelToValue('Type', labelNodes);
  info.product_details = labelToValue('Product Details', labelNodes);
  info.opportunity_owner = labelToValue('Opportunity Owner', labelNodes);
  info.close_date = labelToValue('Close Date', labelNodes);
  info.stage = labelToValue('Stage', labelNodes);
  info.quote_start_date = labelToValue('Quote Start Date', labelNodes);
  info.subscription_end_date = labelToValue('Subscription End Date', labelNodes);
  info.opportunity_term = labelToValue('Opportunity Term', labelNodes);
  info.parent_opportunity = labelToValue('Parent Opportunity', labelNodes);
  return pasteSfdcOpportunity(info);
}

function pasteSfdcOpportunity(info) {
  let text =
    `URL = ${info.url}\n` +
    `Account Name = ${info.account_name}\n` +
    `Type = ${info.type}\n` +
    `Product Details = ${info.product_details}\n` +
    `Close Date = ${info.close_date}\n` +
    `Stage = ${info.stage}\n` +
    `Quote Start Date = ${info.quote_start_date}\n` +
    `Subscription End Date = ${info.subscription_end_date}\n` +
    `Opportunity Term = ${info.opportunity_term}\n` +
    `Parent Opportunity = ${info.parent_opportunity}\n` +
    `Opportunity Owner = ${info.opportunity_owner}\n`
  ;
  return text;
}
