'use strict';

function scrapeSfdc(url) {
  let bodyClasses = document.querySelector('body').classList;
  if (bodyClasses.contains('opportunityTab')) {
    return scrapeSfdcOpportunity(url);
  } else if (bodyClasses.contains('accountTab')) {
    return scrapeSfdcAccount(url);
  } else if (bodyClasses.contains('customnotabTab')) {
    return scrapeSfdcSubscription(url);
  }
}
