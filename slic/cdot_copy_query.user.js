// ==UserScript==
// @name         [cDot] Duplicate Query On Different Model
// @namespace    http://gitlab.com/rrelax
// @version      0.1
// @description  Create a URL to open the current search query on another cDot model type
// @author       @rrelax
// @match        https://customers.gitlab.com/admin/customer?*
// @match        https://customers.gitlab.com/admin/license?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gitlab.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var expectedNavbarCandidates = null;
    var buttonLabelText = "";
    var path = window.location.pathname;
    var modelType = null;
    var targetModelType = null;

    if(path.indexOf("admin/license") != -1) { // License model query
        modelType = "license";
        expectedNavbarCandidates = 2;
        targetModelType = "customer";
    } else if (path.indexOf("admin/customer") != -1) {
        modelType = "customer";
        expectedNavbarCandidates = 1;
        targetModelType = "license";
    }

    if (modelType != null ) {
        buttonLabelText = `Search ${targetModelType}`;

        var navbarCandidates = document.getElementsByClassName("nav nav-tabs");
        if(navbarCandidates.length == expectedNavbarCandidates ) {   //TODO: replace with an assert & try/except clause
            var navBar = navbarCandidates[0];
            var newNavButton = document.createElement("li");
            newNavButton.className = "icon";

            var innerElement = document.createElement("a");
            innerElement.href = "";
            const urlSearchParams = new URLSearchParams(window.location.search);
            const params = Object.fromEntries(urlSearchParams.entries());
            let query = params['query'];
            let newUrl = `https://customers.gitlab.com/admin/${targetModelType}?model_name=${targetModelType}&query=${query}`;
            innerElement.href = newUrl;

            innerElement.onclick = (event) => {
                window.open(newUrl, '_blank').focus();
                event.preventDefault();
            };

            var anchorLabel = document.createElement("span");
            anchorLabel.innerHTML = buttonLabelText;
            innerElement.appendChild(anchorLabel);
            newNavButton.appendChild(innerElement);
            navBar.appendChild(newNavButton);
        } else {
            //Do nothing in case the userscript screws up the page...
            console.log(`[FAULT]\tNavbar candidates found: ${navbarCandidates.length}, expected: ${expectedNavbarCandidates} - modelType: ${modelType}`);
        }
    }

    

})();
