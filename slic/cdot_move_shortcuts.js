// ==UserScript==
// @name         [cDot] Move cDot shortcuts
// @namespace    https://gitlab.com/rrelax
// @version      0.1
// @description  Move each row's shortcuts to under the second/third column for convenience
// @author       @rrelax
// @match        https://customers.gitlab.com/admin/customer?*
// @match        https://customers.gitlab.com/admin/license?*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=gitlab.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    var shortcutGroups = document.getElementsByClassName("last links");
    var path = window.location.pathname;

    let targetClass = null;
    let modelType = null;

    if(path.indexOf("admin/license") != -1) { // License model query
        modelType = "license";
        targetClass = "company_field string_type";

    } else if (path.indexOf("admin/customer") != -1) {
        modelType = "customer";
        targetClass = "email_field string_type";
    }

    if (targetClass != null) {
        var destinationEls = document.getElementsByClassName(targetClass);

        for( var index = 0 ; index < shortcutGroups.length ; index++) {
            var group = shortcutGroups[index];
            var companyLabel = destinationEls[index+1];
            companyLabel.appendChild(group);
        }
    }
})();
