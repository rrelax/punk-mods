// ==UserScript==
// @name          SFDC Hayfever
// @version       1.0.0
// @author        Rene Verschoor
// @description   SFDC skin
// @match         https://gitlab.my.salesforce.com/*
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/sfdc_hayfever
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/sfdc_hayfever/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/sfdc_hayfever/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/sfdc_hayfever/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const BG = 'White';  // Need to specify inverse color

(function() {
  GM_addStyle(`html { filter: invert(100%) contrast(.8) hue-rotate(180deg) !important; background: ${BG} !important; }`);
})();
