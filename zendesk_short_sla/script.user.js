// ==UserScript==
// @name          Zendesk short SLA
// @version       1.1
// @author        Rene Verschoor
// @description   Zendesk: shorten SLA header
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_short_sla
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_short_sla/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const MINIMUM_NR_COLUMS = 4;
const SELECTOR_HEADER = '[data-test-id="generic-table"] > thead > tr > th';
const TITLE_OLD = 'Next SLA breach';
const TITLE_NEW = 'SLA';

  (function() {

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver( mutations => {
    po(mutations);
  });
  observer.observe(document.body, { childList: true, subtree: true });

  function po(mutations) {
    let ths = document.querySelectorAll(SELECTOR_HEADER);
    if (ths.length >= MINIMUM_NR_COLUMS) {
      ths.forEach(th => {
        if (th.textContent === TITLE_OLD)
          th.textContent = TITLE_NEW;
      })
    }
  }

})();
