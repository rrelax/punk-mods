# LicenseDot Hayfever

## Purpose

A userscript to skin LicenseDot with a dark theme.

This summer is one of the worst ever for my hayfever. \
I survive by working in an almost dark room to avoid having my eyes getting more agitated. \
That's ok-ish, except for the fact that I have to work a lot with LicenseDot, which sandblasts my eyes.

Time to slap a dark theme on LicenseDot.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/licensedot_hayfever/script.user.js

## Configuration

No configuration (yet?).

## Technical

I'm using the blunt `filter: invert` on `html` approach. \
For a simple styled site that works good enough.

## Changelog

- 1.0.0
  - Initial release

