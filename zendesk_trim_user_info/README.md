# Zendesk trim user info

## Purpose

A userscript for Zendesk tickets.

This script removes duplicate and standard list entries of the internal User Info comment. \
This compacts the ticket, making it a bit more readable, and have you scroll less.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_user_info/script.user.js

## Technical

The script uses a `MutationObserver` to watch for changes on the Zendesk page. Normally you would hook to e.g. the `DOMContentLoaded` event, but I couldn't get that working with Zendesk. The script was not triggered when another ticket was loaded in a tab. My guess is that this is caused by Zendesk building dynamic pages using Ember. Using `MutationObserver` solved it, at the cost of unneeded triggers of the script.

## Changelog

- 1.0.0
  - Public release
