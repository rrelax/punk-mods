# Zuora Hayfever

## Purpose

A userscript to skin Zuora with a dark theme.

This summer is one of the worst ever for my hayfever. \
I survive by working in an almost dark room to avoid having my eyes getting more agitated. \
That's ok-ish, except for the fact that I have to work a lot with Zuora, which sandblasts my eyes.

Time to slap a dark theme on Zuora.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zuora_hayfever/script.user.js

## Configuration

No configuration (yet?).

## Technical

I'm using the blunt `filter: invert` on `html` approach. \
For a simple styled site that works good enough.

## Changelog

- 1.0.0
  - Initial release

