// ==UserScript==
// @name         [ZD][beta] Shortcut to cDot Customer Search
// @namespace    https://gitlab.com/rrelax/punk-mods/
// @version      0.2
// @description  Adds a shortcut anchor to ZD tickets for searching cDot with the ticket requester's subscription email
// @author       @rrelax
// @match        https://gitlab.zendesk.com/agent/tickets/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zendesk.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    console.log("[Userscript]\tAdding delayed function to run one page finishes rendering...");
    setTimeout( () => {
    console.log("[Userscript]\tStarting delayed function...");

    let foundLabelTag = null;
    for (const labelTag  of document.querySelectorAll("label")){
        if (labelTag.textContent.includes("Subscription Email")){
            console.log(labelTag);
            foundLabelTag = labelTag;
        }
    }
    if (foundLabelTag != null) {
        let customerEmail = foundLabelTag.nextSibling.value;
        let newUrl = `https://customers.gitlab.com/admin/customer?model_name=customer&query=${customerEmail}`;
        console.log(`[Userscript]\tGenerated URL: ${newUrl}`);

        let existingButtons = document.querySelectorAll('[data-test-id="assignee-field-take-it-button"]');

        if(existingButtons.length>0){
            let template = existingButtons[0];

            let newTag = template.cloneNode();

            newTag.innerHTML = "Search cDot";
            let wrappingAnchor = document.createElement("a");
            wrappingAnchor.setAttribute("target", "_blank");

            wrappingAnchor.href = newUrl;
            wrappingAnchor.appendChild(newTag);
            foundLabelTag.parentNode.appendChild(wrappingAnchor);

        }
    }
    }, 5000);
})();
