# Zendesk hide signature

## Purpose

A userscript for Zendesk tickets.

This script hides the agent signature and feedback line in all comments. \
This compacts the ticket, making it a bit more readable, and have you scroll less.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_signature/script.user.js

## Technical

The script uses a `MutationObserver` to watch for changes on the Zendesk page. Normally you would hook to e.g. the `DOMContentLoaded` event, but I couldn't get that working with Zendesk. The script was not triggered when another ticket was loaded in a tab. My guess is that this is caused by Zendesk building dynamic pages using Ember. Using `MutationObserver` solved it, at the cost of unneeded triggers of the script.

## Changelog

- 1.0.2
  - Hides **all** agent signatures
  - No configuration needed
- 1.0.1
  - Fixed @match
- 1.0 
  - Public release
